var express = require('express');
var http = require('http');

var app = new express();

app.get('/health', (req, res) => {
    res.send('OK');
    res.end();
});

app.use('/', function (req, res) {
    res.json({
        message: 'Welcome to nodejs express API!'
    });

    res.end();
});

var server = http.createServer(app);

server.listen(1234, function () {
    console.log('Server listening on port:1234');
});